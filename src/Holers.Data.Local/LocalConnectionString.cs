﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.Local
{
    public class LocalConnectionString : BaseConnectionString, IConnectionString
    {
        public LocalConnectionString(string scheme = "data")
            : base("file", "", 0)
        {
            this.Scheme = scheme;
        }
    }
}
