﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.Local
{
    public class LocalDatabaseCollection : BaseDatabaseCollection<Guid, object>, IDatabaseCollection<Guid, object>
    {
        protected virtual DirectoryInfo DataPath { get; set; }
        public override IDatabase<Guid, object> this[string name]
        {
            get
            {
                IDatabase<Guid, object> result = null;
                if (!Exists(name))
                    Databases.Add(name, new LocalDatabase(DataPath.FullName + "\\" + name));
                Databases.TryGetValue(name, out result);
                return result;
            }
        }
        public override bool Exists(string name)
        {
            return base.Exists(name);
        }
        public LocalDatabaseCollection(DirectoryInfo dataPath)
        {
            DataPath = dataPath;
        }
    }
}
