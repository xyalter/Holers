﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.Local
{
    public class LocalServer : BaseServer<Guid, object>, IServer<Guid, object>
    {
        protected virtual DirectoryInfo DataPath { get; set; }

        public LocalServer(IConnectionString connectionString = null)
        {
            if (connectionString == null)
                ConnectionString = new LocalConnectionString();
            else
                ConnectionString = connectionString;

            DataPath = new DirectoryInfo(ConnectionString.Scheme);
            Databases = new LocalDatabaseCollection(DataPath);
        }
    }
}