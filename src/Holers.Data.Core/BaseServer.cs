﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.Core
{
    public class BaseServer<Id, Value> : IServer<Id, Value>
    {
        public IConnectionString ConnectionString { get; protected set; }
        public IDatabaseCollection<Id, Value> Databases { get; protected set; }

        public BaseServer(IConnectionString connectionString = null)
        {
            if (connectionString == null)
                ConnectionString = new BaseConnectionString();
            else
                ConnectionString = connectionString;
        }
    }
}
