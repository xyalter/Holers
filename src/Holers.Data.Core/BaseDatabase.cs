﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.Core
{
    public class BaseDatabase<Id, Value> : IDatabase<Id, Value>
    {
        protected Hashtable collections;
        public virtual IRepository<Entity, Id, Value> GetRepository<Entity>() where Entity : IDataObject<Id>
        {
            string name = typeof(Entity).Name;
            if (!Exists(name))
                collections.Add(name, new BaseRepository<Entity, Id, Value>());
            return (IRepository<Entity, Id, Value>)collections[name];
        }
        public virtual bool Exists(string name)
        {
            return collections.ContainsKey(name);
        }
        public BaseDatabase()
        {
            collections = new Hashtable();
        }
    }
}
