﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.Core
{
    public class BaseConnectionString : IConnectionString
    {
        public virtual string Scheme   { get; set; }
        public virtual string Host { get; set; }
        public virtual int Port { get; set; }
        public BaseConnectionString(string scheme = "", string host = "localhost", int port = 0)
        {
            Scheme = scheme;
            Host = host;
            Port = port;
        }
    }
}