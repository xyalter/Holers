﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.Core
{
    public class BaseRepository<Entity, Id, Value> : IRepository<Entity, Id, Value> where Entity : IDataObject<Id>
    {
        public virtual void Add(Entity obj)
        {
            throw new NotImplementedException();
        }

        public virtual void Del(Id id)
        {
            throw new NotImplementedException();
        }

        public virtual void Del(Entity obj)
        {
            throw new NotImplementedException();
        }

        public virtual void Update(Entity obj)
        {
            throw new NotImplementedException();
        }

        public virtual Entity Get(Id id)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<Entity> Get(string name, Value value)
        {
            throw new NotImplementedException();
        }


        public virtual IEnumerable<Entity> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
