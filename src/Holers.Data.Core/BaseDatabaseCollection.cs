﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.Core
{
    public class BaseDatabaseCollection<Id, Value> : IDatabaseCollection<Id, Value>
    {
        protected Dictionary<string, IDatabase<Id, Value>> Databases { get; set; }
        public virtual IDatabase<Id, Value> this[string name]
        {
            get
            {
                IDatabase<Id, Value> result = null;
                if (!Exists(name))
                    Databases.Add(name, new BaseDatabase<Id, Value>());
                Databases.TryGetValue(name, out result);
                return result;
            }
        }
        public virtual bool Exists(string name)
        {
            return Databases.ContainsKey(name);
        }

        public IEnumerator<IDatabase<Id, Value>> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public BaseDatabaseCollection()
        {
            Databases = new Dictionary<string, IDatabase<Id, Value>>();
        }
    }
}