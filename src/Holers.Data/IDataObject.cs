﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data
{
    public interface IDataObject<IdType>
    {
        IdType Id { get; }
    }
}
