﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data
{
    public interface IDatabaseCollection<Id, Value> : IEnumerable<IDatabase<Id, Value>>
    {
        IDatabase<Id, Value> this[string name] { get; }
        bool Exists(string name);
    }
}