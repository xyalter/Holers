﻿using StarOrigin.Holers.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data
{
    public interface IConnectionString
    {
        /// <summary>
        /// 架构
        /// </summary>
        string Scheme { get; set; }
        /// <summary>
        /// 主机
        /// </summary>
        string Host { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        int Port { get; set; }
    }

    public static class IConnectionStringExt
    {
        /// <summary>
        /// 转换为Uri
        /// </summary>
        /// <param name="connStr">连接字符串接口</param>
        /// <returns></returns>
        public static Uri ToUri(this IConnectionString connStr)
        {
            var builder = new UriBuilder(connStr.Scheme, connStr.Host, connStr.Port);
            return builder.Uri;
        }
        /// <summary>
        /// 转换为字符串
        /// </summary>
        /// <param name="connStr">连接字符串接口</param>
        /// <returns></returns>
        public static string ToString(this IConnectionString connStr)
        {
            return connStr.ToUri().AbsoluteUri;
        }
    }
}
