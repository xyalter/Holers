﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data
{
    public interface IServer<Id, Value>
    {
        IConnectionString ConnectionString { get; }
        IDatabaseCollection<Id, Value> Databases { get; }
    }
}