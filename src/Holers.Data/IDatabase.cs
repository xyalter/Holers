﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data
{
    public interface IDatabase<Id, Value>
    {
        IRepository<Entity, Id, Value> GetRepository<Entity>() where Entity : IDataObject<Id>;
        bool Exists(string name);
    }
}