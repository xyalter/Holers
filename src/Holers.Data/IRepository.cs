﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data
{
    public interface IRepository<EntityType, IdType, Value> where EntityType : IDataObject<IdType>
    {
        void Add(EntityType obj);
        void Del(IdType id);
        void Del(EntityType obj);
        void Update(EntityType obj);
        EntityType Get(IdType id);
        IEnumerable<EntityType> Get(string name, Value value);
        IEnumerable<EntityType> GetAll();
    }
}
