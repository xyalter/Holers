﻿using MongoDB.Bson;
using MongoDB.Driver;
using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.MongoDb
{
    public class MongoDbServer : BaseServer<ObjectId, BsonValue>, IServer<ObjectId, BsonValue>
    {
        public virtual IMongoClient Client { get; protected set; }
        public MongoDbServer(IConnectionString connectionString = null)
        {
            if (connectionString == null)
                connectionString = new MongoDbConnectionString();
            ConnectionString = connectionString;

            Client = new MongoClient(ConnectionString.ToString());
            AppDomain.CurrentDomain.SetData("MongoClt", Client);

            Databases = new MongoDbDatabaseCollection();
        }
    }
}
