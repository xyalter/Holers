﻿using MongoDB.Bson;
using MongoDB.Driver;
using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.MongoDb
{
    public class MongoDbRepository<Entity> : BaseRepository<Entity, ObjectId, BsonValue>, IRepository<Entity, ObjectId, BsonValue> where Entity : IDataObject<ObjectId>
    {
        private IMongoCollection<Entity> collection;

        public MongoDbRepository(IMongoCollection<Entity> collection)
        {
            this.collection = collection;
        }

        public override void Add(Entity obj)
        {
            collection.InsertOne(obj);
        }

        public override void Del(ObjectId id)
        {
            collection.DeleteOne((x)=>x.Id==id);
        }

        public override void Del(Entity obj)
        {
            PropertyInfo pi = typeof(Entity).GetProperty("Id");
            ObjectId target = (ObjectId)pi.GetValue(obj);
            Del(target);
        }

        public override void Update(Entity obj)
        {
            PropertyInfo pi = typeof(Entity).GetProperty("Id");
            ObjectId target = (ObjectId)pi.GetValue(obj);
            collection.ReplaceOne((x) => x.Id == target, obj);
        }

        public override Entity Get(ObjectId id)
        {
            return collection.Find((x) => x.Id == id).FirstOrDefault();
        }

        public override IEnumerable<Entity> Get(string name, BsonValue value)
        {
            var filter =  Builders<Entity>.Filter.Eq(name, value);
            return collection.Find(filter).ToEnumerable();
        }

        public override IEnumerable<Entity> GetAll()
        {
            return collection.Find(new BsonDocument()).ToEnumerable();
        }
    }
}
