﻿using MongoDB.Bson;
using MongoDB.Driver;
using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.MongoDb
{
    public class MongoDbDatabaseCollection : BaseDatabaseCollection<ObjectId, BsonValue>, IDatabaseCollection<ObjectId, BsonValue>
    {
        protected IMongoClient Client { get; set; }
        public override IDatabase<ObjectId, BsonValue> this[string name]
        {
            get
            {
                IDatabase<ObjectId, BsonValue> result = null;
                if (!Databases.ContainsKey(name))
                {
                    if (!Exists(name))
                    {
                        throw new Exception();
                    }
                    Databases.Add(name, new MongoDbDatabase(Client.GetDatabase(name)));
                }
                Databases.TryGetValue(name, out result);
                return result;
            }
        }
        public override bool Exists(string name)
        {
            return true;
        }
        public MongoDbDatabaseCollection()
        {
            Client = (IMongoClient)AppDomain.CurrentDomain.GetData("MongoClt"); ;
        }
    }
}
