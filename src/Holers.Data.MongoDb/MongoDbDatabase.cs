﻿using MongoDB.Bson;
using MongoDB.Driver;
using StarOrigin.Holers.Data.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.MongoDb
{
    public class MongoDbDatabase : BaseDatabase<ObjectId, BsonValue>, IDatabase<ObjectId, BsonValue>
    {
        private IMongoDatabase database;

        public MongoDbDatabase(IMongoDatabase database)
        {
            this.database = database;
        }

        public override IRepository<T, ObjectId, BsonValue> GetRepository<T>()
        {
            string name = typeof(T).Name;
            if (!collections.ContainsKey(name))
            {
                if (!Exists(name))
                {
                    database.CreateCollection(name);
                }
                collections.Add(name, new MongoDbRepository<T>(database.GetCollection<T>(name)));
            }
            return (IRepository<T, ObjectId, BsonValue>)collections[name];
        }
        public override bool Exists(string name)
        {
            return true;
        }
    }
}