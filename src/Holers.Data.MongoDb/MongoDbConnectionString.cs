﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.MongoDb
{
    public class MongoDbConnectionString : BaseConnectionString, IConnectionString
    {
        public MongoDbConnectionString(string host = "localhost", int port = 27017)
            : base("mongodb", host, port)
        {
        }
    }
}
