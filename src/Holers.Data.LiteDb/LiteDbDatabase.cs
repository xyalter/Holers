﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.LiteDb
{
    public class LiteDbDatabase : BaseDatabase<Guid, object>, IDatabase<Guid, object>
    {
        protected virtual DirectoryInfo DataPath { get; set; }

        protected Hashtable Repositories { get; set; }

        public LiteDbDatabase(string path)
        {
            DataPath = new DirectoryInfo(path);
        }

        public override IRepository<T, Guid, object> GetRepository<T>()
        {
            string name = typeof(T).Name;
            if (!collections.ContainsKey(name))
                collections.Add(name, new LiteDbRepository<T>(DataPath.FullName + "\\" + name));
            return (IRepository<T, Guid, object>)collections[name];
        }
    }
}
