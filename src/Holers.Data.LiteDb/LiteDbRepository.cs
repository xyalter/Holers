﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.LiteDb
{
    public class LiteDbRepository<T> : BaseRepository<T, Guid, object>, IRepository<T, Guid, object> where T : IDataObject<Guid>
    {
        protected virtual DirectoryInfo DataPath { get; set; }
        protected virtual ICollection<T> Objects { get; set; }

        public override void Add(T obj)
        {
            Objects.Add(obj);
        }

        public override void Del(Guid id)
        {
            Objects.Remove(Get(id));
        }

        public override void Del(T obj)
        {
            Del(obj.Id);
        }

        public override void Update(T obj)
        {
            Del(obj.Id);
            Add(obj);
        }

        public LiteDbRepository(string path)
        {
            DataPath = new DirectoryInfo(path);
            if (!DataPath.Exists)
            {
                DataPath.Create();
                Objects = new Collection<T>();
            }
            else
            {
                FileInfo fi = new FileInfo(DataPath.FullName + "\\" + "data");
                if (fi.Exists)
                {
                    FileStream fs = fi.OpenRead();
                    BinaryFormatter formatter = new BinaryFormatter();
                    Objects = (Collection<T>)formatter.Deserialize(fs);
                }
                else
                {
                    Objects = new Collection<T>();
                }
            }
        }

        public override T Get(Guid id)
        {
            return Objects.Single(x => x.Id.Equals(id));
        }

        public override IEnumerable<T> Get(string name, object value)
        {
            Type type = typeof(T);
            PropertyInfo pi = type.GetProperty(name);
            return Objects.Where(x => pi.GetValue(x).Equals(value));
        }

        public override IEnumerable<T> GetAll()
        {
            return Objects;
        }

        public virtual void Commit()
        {
            FileInfo fi = new FileInfo(DataPath.FullName + "\\" + "data");

            if (fi.Exists)
            { fi.Delete(); }

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = fi.Open(FileMode.CreateNew);
            formatter.Serialize(fs, Objects);
            fs.Flush();
            fs.Close();
        }
    }
}
