﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.LiteDb
{
    public class LiteDbServer : BaseServer<Guid, object>, IServer<Guid, object>
    {
        protected virtual DirectoryInfo DataPath { get; set; }

        public LiteDbServer(IConnectionString connectionString = null)
        {
            if (connectionString == null)
                ConnectionString = new LiteDbConnectionString();
            else
                ConnectionString = connectionString;

            DataPath = new DirectoryInfo(ConnectionString.Scheme);
            Databases = new LiteDbDatabaseCollection(DataPath);
        }
    }
}