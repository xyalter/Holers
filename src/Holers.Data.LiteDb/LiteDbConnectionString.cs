﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.LiteDb
{
    public class LiteDbConnectionString : BaseConnectionString, IConnectionString
    {
        public LiteDbConnectionString(string scheme = "data")
            : base("file", "", 0)
        {
            this.Scheme = scheme;
        }
    }
}
