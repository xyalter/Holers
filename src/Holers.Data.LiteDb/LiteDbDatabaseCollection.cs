﻿using StarOrigin.Holers.Data.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarOrigin.Holers.Data.LiteDb
{
    public class LiteDbDatabaseCollection : BaseDatabaseCollection<Guid, object>, IDatabaseCollection<Guid, object>
    {
        protected virtual DirectoryInfo DataPath { get; set; }

        public override IDatabase<Guid, object> this[string name]
        {
            get
            {
                IDatabase<Guid, object> result = null;
                if (!Databases.ContainsKey(name))
                    Databases.Add(name, new LiteDbDatabase(DataPath.FullName + "\\" + name));
                Databases.TryGetValue(name, out result);
                return result;
            }
        }

        public LiteDbDatabaseCollection(DirectoryInfo dataPath)
        {
            DataPath = dataPath;
        }
    }
}
