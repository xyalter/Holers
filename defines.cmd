set dxver=v0.01
set configuration=Debug
set vsver=vs2015

set ProgramFiles64bit=C:\Program Files (x86)\
set ProgramFiles32bit=C:\Program Files\

rem If you are running x64 Windows, uncomment the following line:
set ProgramFiles32bit=%ProgramFiles64bit%

set GACPATH="%WinDir%\assembly\GAC_MSIL\"
set Gac4path="%WinDir%\Microsoft.NET\assembly\GAC_MSIL\"

if '%vsver%'=='vs2010' goto vs2010
if '%vsver%'=='vs2015' goto vs2015

:vs2010
set sn="%ProgramFiles32bit%\Microsoft SDKs\Windows\v7.0A\Bin\sn.exe"
set gacutil="%ProgramFiles32bit%\Microsoft SDKs\Windows\v7.0A\Bin\NETFX 4.0 Tools\gacutil.exe"
set msbuild="%WinDir%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe" 
goto end

:vs2015
set sn="%ProgramFiles32bit%\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\sn.exe"
set gacutil="%ProgramFiles32bit%\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\gacutil.exe"
set msbuild="%WinDir%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
rem set msbuild="%ProgramFiles32bit%\MSBuild\14.0\Bin\MSBuild.exe"
set pa="%ProgramFiles32bit%\Microsoft Visual Studio 14.0\Common7\IDE\PublicAssemblies"
goto end

:end