cd src

for /F "delims==" %%i in ('dir /b /ad') do (
   cd %%i
   rd /S /Q bin
   rd /S /Q obj
   cd ..
)

cd ..